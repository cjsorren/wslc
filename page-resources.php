<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>


<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">



<section class="news content-wrapper">

<!--Teachers Section -->

<?php
/*
 * Loop through Categories and Display Posts within
 */
$post_type = 'resources';

// Get all the taxonomies for this post type
$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );

foreach( $taxonomies as $taxonomy ) :

    // Gets every "category" (term) in this taxonomy to get the respective posts
    $terms = get_terms( $taxonomy );

    foreach( $terms as $term ) : ?>

      <h2><?php echo $term->name; ?></h2>
      <ul>

        <?php
        $args = array(
                'post_type' => $post_type,
                'posts_per_page' => -1,  //show all posts
                'tax_query' => array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field' => 'slug',
                        'terms' => $term->slug,
                    )
                )

            );
        $posts = new WP_Query($args);

        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post(); ?>
            <li>
                <?php if( get_field('select_type') == 'link' ): ?>
                    <a href="<?php the_field( 'link' ); ?>">

                <?php else: ?>
                    <a href="<?php the_field( 'upload_file' ); ?>">

                <?php endif; ?>

                <?php the_field( 'description' ); ?>
                    </a>
            </li>
        <?php endwhile; endif; ?>
        </ul>
    <?php endforeach;


endforeach; ?>



</section>






<?php
get_footer();