<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>

<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">

<section class="contact content-wrapper">
    <div class="contact-info">
        <p>
            <strong><?php the_field( 'name' ); ?></strong><br>
            <?php the_field( 'address_line_1' ); ?><br>
            <?php the_field( 'address_line_2' ); ?><br>
            <?php the_field( 'phone_number' ); ?><br>
            <a href="mailto:<?php the_field( 'email_address' ); ?>"><?php the_field( 'email_address' ); ?></a><br>

        </p>
    </div>

</section>



<?php
get_footer();