<?php
/**
 * Template part for displaying hero section.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

?>

<section class="hero site-header">

    <?php if (CFS()->get( 'header_image' )): ?>
        <div class="overlay-wrapper">
        <div class="header-overlay">
        <img class="header-image" src="<?php echo CFS()->get( 'header_image' ); ?>">
        </div>
        </div>
    <?php else: ?>
    <?php endif; ?>

    <div class="hero__content content-wrapper">
        <span><?php echo CFS()->get( 'page_title' ); ?></span>

    </div>

</section>