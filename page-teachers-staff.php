<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>




<!--

<style>
.teachers img {
    display:block;
    width: 100%;
    max-width:200px;
    margin:0;
}
.hero {
  position:relative;
  width:100%;
}
.hero__content {
  top: 70%;
  width:214px;
  background: rgba(115,159,162,0.80);
  font-family: Montserrat;
  font-weight:bold;
  font-size: 18px;
  color: #FFFFFF;
  letter-spacing: 0.99px;


  position: absolute;
  transform: translateY(-30%);
  text-align: center;
}

@media screen and (min-width: 697px) {
    .hero__content {
      top: 70%;
      width:428px;
      background: rgba(115,159,162,0.80);
      font-family: Montserrat;
      font-weight:bold;
      font-size: 40px;
      color: #FFFFFF;
      letter-spacing: 0.99px;
      position: absolute;
      transform: translateY(-30%);
      text-align: center;
    }

    .teachers {
        width:100%;
        height: auto;

        margin:0 auto;
    }
    .float-left {
        display:block;
        width:20%;
        float:left;
    }
    .teachers img {

        max-width:200px;
    }
    .teacher-description {
      display: block;
        float:right;
        width:70%;
    }


}



</style>

-->

<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">



<section class="teachers content-wrapper">



        <!--Teachers Section -->
        <?php

        $args= array (
          'post_type' => 'faculty_staff',
          'meta_query' => array(
                  array(
                      'key' => 'faculty_group', // name of custom field
                      'value' => 'faculty', // matches exactly "red"
                      'compare'   => 'LIKE'
                  )
              )

        );

        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>
          <h2>Teachers &amp; Staff</h2>
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="float-left">
              <img src="<?php the_field( 'teacher_image' ); ?>" alt="<?php the_field( 'faculty_name' ); ?>">
            </div>
            <div class="teacher-description">
                <h3><?php the_field( 'faculty_name' ); ?></h3>
                <h4><?php the_field( 'position' ); ?></h4>
                <span><?php the_field( 'teacher_description' ); ?></span>
                <a href="#"><?php the_field( 'faculty_email' ); ?></a>

            </div>
          <?php endwhile; else: ?>
            No Posts to show
        <?php endif; ?>

        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

</section>

<!--Administrative Staff -->
<section class="secondary-faculty">
        <?php

        $args= array (
          'post_type' => 'faculty_staff',
          'meta_query' => array(
                  array(
                      'key' => 'faculty_group', // name of custom field
                      'value' => 'administration', // matches exactly "red"
                      'compare'   => 'LIKE'
                  )
              )

        );

        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>
          <ul>
          <h2>Administrative Staff</h2>
          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>


                <li><span class="secondary-faculty-name"><?php the_field( 'faculty_name' ); ?>, </span><span class="secondary-faculty-position"><i><?php the_field( 'position' ); ?></i></span></li>



          <?php endwhile; else: ?>
            No Posts to show
        <?php endif; ?>
        </ul>
        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>
</section>

<!--Board of Directors -->
<section class="secondary-faculty">
        <?php

        $args= array (
          'post_type' => 'faculty_staff',
          'meta_query' => array(
                  array(
                      'key' => 'faculty_group', // name of custom field
                      'value' => 'board', // matches exactly "red"
                      'compare'   => 'LIKE'
                  )
              )

        );

        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>
            <h2>Board of Directors</h2>
            <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

              <div class="secondary-faculty-description">
                  <h3><?php the_field( 'faculty_name' ); ?></h3>
                  <h4><?php the_field( 'position' ); ?></h4>
              </div>
        <?php endwhile; else: ?>
              No Posts to show
        <?php endif; ?>

        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

</section>





<?php
get_footer();