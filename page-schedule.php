<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>

<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">




<section>

<!--
<div class="schedule-filters">
    <ul>
        <li><a href="" class="btn-tag">Morning</a></li>
        <li><a href="" class="btn-tag">Afernoon</a></li>
        <li><a href="" class="btn-tag">Evening</a></li>
    </ul>
</div>
-->

<table class="schedule schedule__morning">
    <caption><span class="schedule-name">Morning </span><span class="schedule-secret-break"><br></span><?php the_field( 'days', 'class_time_11' ); ?> <?php the_field( 'time', 'class_time_11' ); ?></caption>
    <thead>
        <tr>
            <th class="align-right">Room</th>
            <th>Class</th>
        </tr>
    </thead>
    <tbody>


        <?php

              $args = array(
                'post_type' => 'class',
                'tax_query'=>array(
                    array(
                    'taxonomy' => 'class_time',
                    'field' => 'id',
                    'terms' => '11'
                    )
                )
              );



        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>

          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <tr>
                <td class="align-right"><?php the_field( 'room_number' ); ?></td>
                <td><?php the_field( 'class_name' ); ?> <span><?php the_field( 'instructor' ); ?></span></td>
            </tr>
          <?php endwhile; else: ?>
            <tr>
              No classes posted yet
            </tr>
        <?php endif; ?>
    </tbody>
</table>

        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

<!--Afternoon Schedule -->

<table class="schedule schedule__afternoon">
    <caption><span class="schedule-name">Afternoon </span><span class="schedule-secret-break"><br></span><?php the_field( 'days', 'class_time_12' ); ?> <?php the_field( 'time', 'class_time_12' ); ?></caption>
    <thead>
        <tr>
            <th class="align-right">Room</th>
            <th>Class</th>
        </tr>
    </thead>
    <tbody>
        <?php

        $args = array(
                        'post_type' => 'class',
                        'tax_query'=>array(
                            array(
                            'taxonomy' => 'class_time',
                            'field' => 'id',
                            'terms' => '12'
                            )
                        )
                      );

        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>

          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <tr>
                <td class="align-right"><?php the_field( 'room_number' ); ?></td>
                <td><?php the_field( 'class_name' ); ?> <span><?php the_field( 'instructor' ); ?></span></td>
            </tr>
          <?php endwhile; else: ?>
            <tr>
              No classes posted yet
            </tr>
        <?php endif; ?>
    </tbody>
</table>

        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

<!--Evening Schedule -->

<table class="schedule schedule__evening">
    <caption><span class="schedule-name">Evening </span><span class="schedule-secret-break"><br></span><?php the_field( 'days', 'class_time_13' ); ?> <?php the_field( 'time', 'class_time_13' ); ?></caption>
    <thead>
        <tr>
            <th class="align-right">Room</th>
            <th>Class</th>
        </tr>
    </thead>
    <tbody>
        <?php

        $args = array(
                        'post_type' => 'class',
                        'tax_query'=>array(
                            array(
                            'taxonomy' => 'class_time',
                            'field' => 'id',
                            'terms' => '13'
                            )
                        )
                      );

        // query
        $the_query = new WP_Query( $args );

        ?>

        <?php if( $the_query->have_posts() ): ?>

          <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <tr>
                <td class="align-right"><?php the_field( 'room_number' ); ?></td>
                <td><?php the_field( 'class_name' ); ?> <span><?php the_field( 'instructor' ); ?></span></td>
            </tr>
          <?php endwhile; else: ?>
            <tr>
              No classes posted yet
            </tr>
        <?php endif; ?>
    </tbody>
</table>

        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>








</section>



<?php
get_footer();