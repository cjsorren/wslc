<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wslc
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="three-col">
        <div class="svg-logo">
            <svg width="147px" height="34px" viewBox="0 0 147 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Footer" transform="translate(-41.000000, -81.000000)" fill="#FFFFFF">
                        <g transform="translate(0.000000, 44.000000)" id="logo">
                            <g transform="translate(41.000000, 37.000000)">
                                <path d="M23.143914,18.2909236 L16.8788508,29.0871165 L0,0 L33.6415143,0 L46.2874944,0 L29.4080027,29.0864788 L23.143914,18.2909236 Z" id="Combined-Shape"></path>
                                <g id="Group-13" transform="translate(45.000000, 0.000000)">
                                    <path d="M7.89839094,21.8403771 C2.92087021,16.0683751 3.10016636,7.78406976 8.30417044,3.32271991 C13.5155597,-1.1436858 21.7925097,-0.086389878 26.7911107,5.68792533 L19.8620195,11.6276755 C24.8387279,17.3996689 24.6606537,25.6838344 19.4567101,30.1457691 C14.2466027,34.6121748 5.96837079,33.5536035 0.969769782,27.7805637 L7.89838911,21.8403787 Z" id="Combined-Shape"></path>
                                    <polygon id="Fill-8" points="38 32.6745874 49.7629599 32.6745874 49.7629599 0.558285303 38 0.558285303"></polygon>
                                    <polygon id="Fill-10" points="38 33.0498576 64.0377184 33.0498576 64.0377184 21 38 21"></polygon>
                                    <path d="M88.7801478,17.7223348 L101.727543,10.2855525 C99.0612811,5.35299275 93.8268185,2 87.8040138,2 C79.0758501,2 72,9.03886154 72,17.7223348 C72,20.7826262 72.8819176,23.6363049 74.4015589,26.0518881 L88.841036,17.7574079 L88.7801478,17.7223348 Z" id="Fill-11"></path>
                                    <path d="M87.4018139,32.688537 C93.4246186,32.688537 98.6590813,29.3349066 101.325985,24.4029846 L88.4388362,17 L74,25.2944802 C76.7938071,29.7341027 81.7494659,32.688537 87.4018139,32.688537" id="Fill-12"></path>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
        </div>
        <p class="footer-address"><strong><?php the_field( 'name', 16); ?></strong><br /><?php the_field( 'address_line_1', 16); ?><br /><?php the_field( 'address_line_2', 16); ?></p>
        <p class="footer-social"><img class ="footer-icon" src="<?php echo get_home_url(); ?>/wp-content/uploads/assets/icon-phone.png"><?php the_field( 'phone_number', 16); ?></p>
        <p class="footer-social"><img class ="footer-icon" src="<?php echo get_home_url(); ?>/wp-content/uploads/assets/icon-email.png"><?php the_field( 'email_address', 16); ?></p>
    </div>

    <div class="six-col col-left">

            <div class="footer-menu-center">
                <h4 class="footer-top-links">ABOUT</h4>
                <div class="footer-menu-container">
                    <ul>
                        <li><a href="<?php echo get_home_url(); ?>/about">Our Story</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/about">Admission</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/news">News</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/calendar">Calendar</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/teachers-staff">Faculty</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/location">Location &amp; Directions</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/contact">Contact</a></li>
                    </ul>
                </div>
            </div>

    </div>

    <div class="six-col col-right">

            <div class="footer-menu-center">
                <h4 class="footer-top-links">PROGRAMS</h4>
                <div class="footer-menu-container">
                    <ul>
                        <li><a href="<?php echo get_home_url(); ?>/programs">ESL</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/programs">VESOL</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/programs">Employment Focus&nbsp;ESL</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-menu-center">
                <h4 class="footer-top-links">STUDENTS</h4>
                <div class="footer-menu-container">
                    <ul>
                        <li><a href="<?php echo get_home_url(); ?>/schedule">Class Schedule</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/resources">Resources</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/jobs">Jobs</a></li>
                    </ul>
                </div>
            </div>

    </div>

    <div class="three-col">
        <div class="footer-menu-center">
            <img src="<?php echo get_home_url(); ?>/wp-content/uploads/assets/scsd-logo.png" alt="Syracuse City School District"><br>
            <span>Syracuse City School District</span>
        </div>
    </div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
