<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>


<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">



<section class="news content-wrapper">

<!--Teachers Section -->
<?php

$args= array (
  'post_type' => 'news'
);

// query
$the_query = new WP_Query( $args );

?>

<?php if( $the_query->have_posts() ): ?>

  <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div>
      <h3><?php the_title(); ?></h3>
      <p><?php the_field( 'news_content' ); ?></p>
    </div>

  <?php endwhile; else: ?>
    No Posts to show
<?php endif; ?>

  <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>


</section>






<?php
get_footer();