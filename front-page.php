<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>

<div class="site-header">
    <img class="header-image" src="<?php echo CFS()->get( 'header_image' ); ?>">
</div>

<div id="content" class="site-content">
<div class="front-page-intro-text">
    <p><?php echo CFS()->get( 'intro_text' ); ?></p>
</div>

<div class="funfacts">
    <div class="fact">
        <strong>37 Countries</strong>
        <p>We have a very diverse group of students</p>
    </div>
    <div class="fact">
        <strong>25 Languages</strong>
        <p>We have a very diverse group of students.</p>
    </div>
    <div class="fact">
        <strong>200</strong>
        <p>Students enrolled in 2015</p>
    </div>

</div>

<div class="purpleblocks">
    <div class="p-left"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2016/06/student1.jpg" alt="Student"></div>
    <div class="p-right"><p><span class="purple-block-text">Our job focused ESL classes prepare students to find great jobs.</p>
    <span class="register-now">REGISTER NOW</span></div>

</div>

<div class="yellowblocks">
    <div class="block1">
        <div class="blockwrap events">
            <span class="block-headline">UPCOMING EVENTS</span>
            <?php
            echo do_shortcode("[ecs-list-events limit='3' thumb='false' contentorder='date,title']");
            ?>

        </div>
    </div>
    <div class="block2">
        <div class="blockwrap news">
            <span class="block-headline">LATEST NEWS</span>
            <ul>
                <li><p class="news-item">Check out the recent pictures from the Pedals to Possibilities group.<br/><span class="news-link">Read More</span></p></li>
                <li><p class="news-item">Heather posted the projects from the Beginning 3 class.<br/><span class="news-link">Read More</span></p></li>
            </ul>
        </div>
    </div>
</div>


<?php
get_footer();