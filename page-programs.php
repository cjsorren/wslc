<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>

<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">
<!--
<section class="hero">
    <img class="header-image" src="<?php echo CFS()->get( 'header_image' ); ?>">

    <div class="hero__content">
        <span><?php echo CFS()->get( 'page_title' ); ?></span>

    </div>

</section>
-->


  <div class="program-block-wrapper">

    <div class="program-block">

            <img src="<?php the_field( 'program_image', 'course_program_10' ); ?>" alt="">
            <div class="program-caption">
            <span><?php the_field( 'image_caption', 'course_program_10' ); ?></span>
            <!--
            <p><?php the_field( 'image_subtext', 'course_program_10' ); ?></p>
            -->
            </div>

        </div>

    <div class="program-block">

        <img src="<?php the_field( 'program_image', 'course_program_9' ); ?>" alt="">
        <div class="program-caption">
        <span><?php the_field( 'image_caption', 'course_program_9' ); ?></span>
        <!--
        <p><?php the_field( 'image_subtext', 'course_program_9' ); ?></p>
        -->
        </div>

    </div>

    <div class="program-block">

            <img src="<?php the_field( 'program_image', 'course_program_8' ); ?>" alt="">
            <div class="program-caption caption-double">
            <span><?php the_field( 'image_caption', 'course_program_8' ); ?></span>
            <!--
            <p><?php the_field( 'image_subtext', 'course_program_8' ); ?></p>
            -->
            </div>

        </div>






  </div>



<!--
<div class="program-block-wrapper">
  <div class="program-block"><img src="<?php the_field( 'program_image' ); ?>"></div>
  <div class="program-block"><img src="<?php the_field( 'program_image' ); ?>"></div>
  <div class="program-block"><img src="<?php the_field( 'program_image' ); ?>"></div>
</div>
-->
<section class="Programs content-wrapper">
<!--ESL Section-->
        <?php



      $args = array(
        'post_type' => 'courses',
        'tax_query'=>array(
            array(
            'taxonomy' => 'course_program',
            'field' => 'id',
            'terms' => '10'
            )
        )
      );

      $the_query = new WP_Query( $args );

      ?>

      <?php if( $the_query->have_posts() ): ?>
          <div class="program">
          <h3 class="program-title"><?php the_field( 'program_name', 'course_program_10' ); ?> <?php the_field( 'program_abbreviation', 'course_program_10' ); ?></h3>
        <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <h4 class="class-title"><?php the_field( 'course_name' ); ?></h4>
          <div class="class-description"><?php the_field( 'course_description' ); ?></div>
        <?php endwhile; else: ?>

            No classes posted yet

      <?php endif; ?>
          </div>
        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

<!--VESOL Section-->
        <?php



      $args = array(
        'post_type' => 'courses',
        'tax_query'=>array(
            array(
            'taxonomy' => 'course_program',
            'field' => 'id',
            'terms' => '9'
            )
        )
      );

      $the_query = new WP_Query( $args );

      ?>

      <?php if( $the_query->have_posts() ): ?>
        <div class="program">
          <h3 class="program-title"><?php the_field( 'program_name', 'course_program_9' ); ?> <?php the_field( 'program_abbreviation', 'course_program_9' ); ?></h3>
        <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <h4 class="class-title"><?php the_field( 'course_name' ); ?></h4>
          <div class="class-description"><?php the_field( 'course_description' ); ?></div>
        <?php endwhile; else: ?>



      <?php endif; ?>
        </div>
        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

<!--VESOL Section-->
      <?php

      $args = array(
        'post_type' => 'courses',
        'tax_query'=>array(
            array(
            'taxonomy' => 'course_program',
            'field' => 'id',
            'terms' => '8'
            )
        )
      );

      $the_query = new WP_Query( $args );

      ?>

      <?php if( $the_query->have_posts() ): ?>
        <div class="program">
          <h3 class="program-title"><?php the_field( 'program_name', 'course_program_8' ); ?> <?php the_field( 'program_abbreviation', 'course_program_8' ); ?></h3>
        <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <h4 class="class-title"><?php the_field( 'course_name' ); ?></h4>
          <div class="class-description"><?php the_field( 'course_description' ); ?></div>
        <?php endwhile; else: ?>



      <?php endif; ?>
        </div>
        <?php wp_reset_query(); // Restore global post data stomped by the_post(). ?>

</section>





<?php
get_footer();