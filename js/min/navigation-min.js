/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
 jQuery(document).ready(function() {
   	  jQuery('body').addClass('js');
 		  var $menu = $('#menu'),
 		  	  $menulink = $('.menu-link'),
 		  	  $menuTrigger = $('.has-subnav > a');

 		$menulink.click(function(e) {
 			e.preventDefault();
 			$menulink.toggleClass('active');
 			$menu.toggleClass('active');
 		});

 		$menuTrigger.click(function(e) {
 			e.preventDefault();
 			var $this = $(this);
 			$this.toggleClass('active').next('ul').toggleClass('active');
 		});

 		});

