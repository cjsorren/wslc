<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>


<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">



<section class="About">
    <div class="overview about-overview">
      <h3>OUR STORY</h3>
      <p>Through innovative, expert teaching, we will provide exceptional educational experiences that encourage and support our students in not only developing their English skills, but also in transforming their lives and the lives of their families.</p>
    </div>


</section>
<section id="admission">
  <div class="admissions">
    <h2 class="statement">To register, the most important thing to do is call for an appointment or show up in person.</h2>
    <p>
      <span class="admissions-phone">(315)435-4013</span><br>
      <span>or <a href="./location">visit us in person</a></span>
    </p>
  </div>
</section>





<?php
get_footer();