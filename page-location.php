<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wslc
 */

get_header(); ?>

<?php get_template_part( 'hero' ); ?>

<div id="content" class="site-content">

<div class="map-wrapper">
    <a href="https://www.google.com/maps/place/Westside+Learning+Center/@43.0435562,-76.1618601,15z/data=!4m5!3m4!1s0x0:0x3bbadd317dbb42e2!8m2!3d43.0435562!4d-76.1618601">
    <img src="https://maps.googleapis.com/maps/api/staticmap?center=43.0435582,-76.1618453&markers=422 Gifford St, Syracuse, NY 13204&zoom=15&size=585x310&maptype=roadmap&key=AIzaSyAHbZ3dWaSsx-AJG4ZjwISbPZA6BpCiW5Q&
style=feature%3Aall%7Celement%3Ageometry.fill%7Chue%3A0x82bfe4%7Csaturation%3A-100%7Cvisibility%3Aon%7C&
style=feature%3Apoi.park%7Celement%3Ageometry.fill|color:0xD9FFDA|saturation:5|%7Cvisibility%3Aon%7C&
style=feature%3Apoi.business%7Celement%3Aall%7Cvisibility%3Aoff%7C&
style=feature%3Aroad.local%7Celement%3Alabels.text.fill%7Chue%3A0x00ffff%7Cvisibility%3Aon%7Ccolor%3A0x1d4f6b%7C&
style=feature%3Aroad.highway%7Celement%3Alabels.text.fill%7Chue%3A0x00ffff%7Cvisibility%3Aon%7Ccolor%3A0x1d4f6b%7C&
style=feature%3Aroad.local%7Celement%3Ageometry.stroke%7Ccolor%3A0xf7fafb%7Cvisibility%3Aoff%7C&
style=feature%3Atransit%7Celement%3Aall%7Cvisibility%3Aoff%7C&
style=feature%3Aroad.arterial%7Celement%3Ageometry.stroke%7Cvisibility%3Aoff%7&
style=feature%3Aroad.arterial%7Celement%3Alabels.text.fill%7Ccolor%3A0x1d4f6b%7C&
style=feature%3Aroad.highway%7Celement%3Ageometry.fill%7Ccolor%3A0x85C0E1%7Cvisibility%3Aoff%7C&
style=feature%3Awater%7Celement%3Ageometry.fill%7Ccolor%3A0xC6E2F2%7Cvisibility%3Aon%7C&
style=feature%3Aroad.local%7Celement%3Ageometry.fill%7Cvisibility%3Aon%7C&
style=feature%3Aroad.local%7Celement%3Alabels.text.stroke%7Cvisibility%3Aoff%7C
"></a></div>
<div class="location-info">
    <span>Westside Learning Center</span><br>
    <span>422 Gifford Street</span><br>
    <span>Syracuse, NY 13204</span>
</div>
</div>
</div>


<?php
get_footer();